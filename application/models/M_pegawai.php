<?php

class m_pegawai extends CI_Model
{
                        
    public function getAll()
    {
        return $this->db->get('masterpegawai')->result_array();
    }
    
    public function autoId()
    {
        $this->db->select("MAX(id)+1 AS id");
        $this->db->from("masterpegawai");
        $query = $this->db->get();
        $id = "";
        if($query->num_rows()>0){
            foreach($query->result() as $k){
                $tmp = ((int)$k->id_max)+1;
                $id = sprintf("%04s", $tmp);
            }
        }else{
            $id = "0001";
        }
    }

    public function tambahData()
    {
        $data = array(
            "id" => $this->input->post('id', true),
            "nip" => $this->input->post('nip', true),
            "nik" => $this->input->post('nik', true),
            "ktp" => $this->input->post('ktp', true),
            "gelarDepan" => $this->input->post('gelarDepan', true),
            "nama" => $this->input->post('nama', true),
            "foto" => $this->input->post('foto', true),
            "gelarBelakang" => $this->input->post('gelarBelakang', true),
            "namaPanggilan" => $this->input->post('namaPanggilan', true),
            "tempatLahir" => $this->input->post('tempatLahir', true),
            "tanggalLahir" => $this->input->post('tanggalLahir', true),
            "jenisKelamin" => $this->input->post('jenisKelamin', true),
            "agama" => $this->input->post('agama', true),
            "alamatKTP" => $this->input->post('alamatKTP', true),
            "provinsiKTP" => $this->input->post('provinsiKTP', true),
            "kotaKTP" => $this->input->post('kotaKTP', true),
            "kecamatanKTP" => $this->input->post('kecamatanKTP', true),
            "kelurahanKTP" => $this->input->post('kelurahanKTP', true),
            "kodeposKTP" => $this->input->post('kodeposKTP', true),
            "alamatTinggal" => $this->input->post('alamatTinggal', true),
            "provinsiTinggal" => $this->input->post('provinsiTinggal', true),
            "kotaTinggal" => $this->input->post('kotaTinggal', true),
            "kecamatanTinggal" => $this->input->post('kecamatanTinggal', true),
            "kelurahanTinggal" => $this->input->post('kelurahanTinggal', true),
            "kodeposTinggal" => $this->input->post('kodeposTinggal', true),
            "email" => $this->input->post('email', true),
            "phone" => $this->input->post('phone', true),
            "mobile" => $this->input->post('mobile', true),
            "statusPerkawinan" => $this->input->post('statusPerkawinan', true),
            "statusWajibPajak" => $this->input->post('statusWajibPajak', true),
            "kk" => $this->input->post('kk', true),
            "scanKK" => $this->input->post('scanKK', true),
            "npwp" => $this->input->post('npwp', true),
            "scanNPWP" => $this->input->post('scanNPWP', true),
            "bpjstk" => $this->input->post('bpjstk', true),
            "bpjskes" => $this->input->post('bpjskes', true),
            "golonganDarah" => $this->input->post('golonganDarah', true),
            "nomorRekamMedik" => $this->input->post('nomorRekamMedik', true),
            "nomorSIM" => $this->input->post('nomorSIM', true),
            "nomorRekening" => $this->input->post('nomorRekening', true),
            "golongan" => $this->input->post('golongan', true),
            "profesi" => $this->input->post('profesi', true),
            "jabatan" => $this->input->post('jabatan', true),
            "sotk" => $this->input->post('sotk', true),
            "mkg" => $this->input->post('mkg', true),
            "gapok" => $this->input->post('gapok', true),
            "unit" => $this->input->post('unit', true),
            "statusKepegawaian" => $this->input->post('statusKepegawaian', true),
            "aktaKematian" => $this->input->post('aktaKematian', true),
            "tanggalPensiun" => $this->input->post('tanggalPensiun', true),
            "tanggalKematian" => $this->input->post('tanggalKematian', true),
            "jenjangKematian" => $this->input->post('jenjangKematian', true),
            "jenjangKarirId" => $this->input->post('jenjangKarirId', true),
            "createdAt" => $this->input->post('createdAt', true),
            "createdBy" => $this->input->post('createdBy', true),
            "updatedAt" => $this->input->post('updatedAt', true),
            "updatedBy" => $this->input->post('updatedBy', true),
            "deletedAt" => $this->input->post('deletedAt', true),
            "deletedBy" => $this->input->post('deletedBy', true),
        );

        $this->db->insert('masterpegawai', $data);
    }
     
    public function hapusData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('masterpegawai');
    }

    public function getById($id)
    {
        return $this->db->get_where('masterpegawai', ['id' => $id])->row_array();
    }

    public function ubahData()
    {
        $data = array(
            "nip" => $this->input->post('nip', true),
            "nik" => $this->input->post('nik', true),
            "ktp" => $this->input->post('ktp', true),
            "gelarDepan" => $this->input->post('gelarDepan', true),
            "nama" => $this->input->post('nama', true),
            "foto" => $this->input->post('foto', true),
            "gelarBelakang" => $this->input->post('gelarBelakang', true),
            "namaPanggilan" => $this->input->post('namaPanggilan', true),
            "tempatLahir" => $this->input->post('tempatLahir', true),
            "tanggalLahir" => $this->input->post('tanggalLahir', true),
            "jenisKelamin" => $this->input->post('jenisKelamin', true),
            "agama" => $this->input->post('agama', true),
            "alamatKTP" => $this->input->post('alamatKTP', true),
            "provinsiKTP" => $this->input->post('provinsiKTP', true),
            "kotaKTP" => $this->input->post('kotaKTP', true),
            "kecamatanKTP" => $this->input->post('kecamatanKTP', true),
            "kelurahanKTP" => $this->input->post('kelurahanKTP', true),
            "kodeposKTP" => $this->input->post('kodeposKTP', true),
            "alamatTinggal" => $this->input->post('alamatTinggal', true),
            "provinsiTinggal" => $this->input->post('provinsiTinggal', true),
            "kotaTinggal" => $this->input->post('kotaTinggal', true),
            "kecamatanTinggal" => $this->input->post('kecamatanTinggal', true),
            "kelurahanTinggal" => $this->input->post('kelurahanTinggal', true),
            "kodeposTinggal" => $this->input->post('kodeposTinggal', true),
            "email" => $this->input->post('email', true),
            "phone" => $this->input->post('phone', true),
            "mobile" => $this->input->post('mobile', true),
            "statusPerkawinan" => $this->input->post('statusPerkawinan', true),
            "statusWajibPajak" => $this->input->post('statusWajibPajak', true),
            "kk" => $this->input->post('kk', true),
            "scanKK" => $this->input->post('scanKK', true),
            "npwp" => $this->input->post('npwp', true),
            "scanNPWP" => $this->input->post('scanNPWP', true),
            "bpjstk" => $this->input->post('bpjstk', true),
            "bpjskes" => $this->input->post('bpjskes', true),
            "golonganDarah" => $this->input->post('golonganDarah', true),
            "nomorRekamMedik" => $this->input->post('nomorRekamMedik', true),
            "nomorSIM" => $this->input->post('nomorSIM', true),
            "nomorRekening" => $this->input->post('nomorRekening', true),
            "golongan" => $this->input->post('golongan', true),
            "profesi" => $this->input->post('profesi', true),
            "jabatan" => $this->input->post('jabatan', true),
            "sotk" => $this->input->post('sotk', true),
            "mkg" => $this->input->post('mkg', true),
            "gapok" => $this->input->post('gapok', true),
            "unit" => $this->input->post('unit', true),
            "statusKepegawaian" => $this->input->post('statusKepegawaian', true),
            "aktaKematian" => $this->input->post('aktaKematian', true),
            "tanggalPensiun" => $this->input->post('tanggalPensiun', true),
            "tanggalKematian" => $this->input->post('tanggalKematian', true),
            "jenjangKematian" => $this->input->post('jenjangKematian', true),
            "jenjangKarirId" => $this->input->post('jenjangKarirId', true),
            "createdAt" => $this->input->post('createdAt', true),
            "createdBy" => $this->input->post('createdBy', true),
            "updatedAt" => $this->input->post('updatedAt', true),
            "updatedBy" => $this->input->post('updatedBy', true),
            "deletedAt" => $this->input->post('deletedAt', true),
            "deletedBy" => $this->input->post('deletedBy', true),
        );

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('masterpegawai', $data);
    }
                        
}
                        
/* End of file M_pegawai.php */
    
                        