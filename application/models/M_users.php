<?php

class m_users extends CI_Model
{
                        
    public function getAll()
    {
        return $this->db->get('users')->result_array();
    }
    
    public function autoId()
    {
        $this->db->select("MAX(id)+1 AS id");
        $this->db->from("users");
        $query = $this->db->get();
        $id = "";
        if($query->num_rows()>0){
            foreach($query->result() as $k){
                $tmp = ((int)$k->id)+1;
                $id = sprintf("%02s", $tmp);
            }
        }else{
            $id = "01";
        }
    }

    public function tambahData()
    {
        $data = array(
            "id" => $this->input->post('id', true),
            "username" => $this->input->post('username', true),
            "fullname" => $this->input->post('fullname', true),
            "password" => md5($this->input->post('password', true)),
            "createdAt" => mdate('%Y-%m-%d'),
            "updatedAt" => mdate('%Y-%m-%d'),
            "deletedAt" => $this->input->post('deletedAt', true),
        );

        $this->db->insert('users', $data);
    }
     
    public function hapusData($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('users');
    }

    public function getById($id)
    {
        return $this->db->get_where('users', ['id' => $id])->row_array();
    }

    public function ubahData()
    {
        $data = array(
            "username" => $this->input->post('username', true),
            "fullname" => $this->input->post('fullname', true),
            "password" => $this->input->post('password', true),
            "updatedAt" => mdate('%Y-%m-%d'),
        );

        $this->db->where('id', $this->input->post('id'));
        $this->db->update('users', $data);
    }
                        
}
                        
/* End of file M_users.php */
    
                        