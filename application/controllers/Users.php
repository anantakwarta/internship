<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Users extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('m_users');
        $this->load->library('form_validation');
    }
    
    public function index()
    {
        $data['judul'] = 'Data User';
        $data['users'] = $this->m_users->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('user/index', $data);
        $this->load->view('templates/footer');
    }

    public function tambah()
    {
        $data['judul'] = 'Form Tambah Data User';
        $data['id'] = $this->m_users->autoId();

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('fullname', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('user/tambah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_users->tambahData();
            $this->session->set_flashdata('flash', 'Ditambahkan');
            redirect('users');
        }
    }

    public function hapus($id)
    {
        $this->m_users->hapusData($id);
        $this->session->set_flashdata('flash', 'Dihapus');
        redirect('users');
    }
        
    public function ubah($id)
    {
        $data['judul'] = 'Form Ubah Data User';
        $data['users'] = $this->m_users->getById($id);

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('fullname', 'fullname', 'required');
        $this->form_validation->set_rules('password', 'password', 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('templates/header', $data);
            $this->load->view('user/ubah', $data);
            $this->load->view('templates/footer');
        } else {
            $this->m_users->ubahData();
            $this->session->set_flashdata('flash', 'Diubah');
            redirect('users');
        }
    }
}
        
    /* End of file  Users.php */
        
                            