<?php 
        
defined('BASEPATH') OR exit('No direct script access allowed');
        
class Pegawai extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('m_pegawai');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['judul'] = 'Data Pegawai';
        $data['pegawai'] = $this->m_pegawai->getAll();
        $this->load->view('templates/header', $data);
        $this->load->view('pegawai/index', $data);
        $this->load->view('templates/footer');
    }
        
}
        
    /* End of file  Pegawai.php */
        
                            