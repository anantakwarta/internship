<div class="container-fluid">
    <div class="row mt-3">
        <div class="col-md-6">

            <div class="card">
                <div class="card-header">
                    Form Tambah Data Pegawai
                </div>
                <div class="card-body">
                    <form action="" method="post">
                        <input type="hidden" name="id" value="<?= $id['id']; ?>">
                        <div class="form-group">
                            <label for="nip">NIP</label>
                            <input type="text" class="form-control" id="nip" name="nip">
                            <small class="form-text text-danger"><?php echo form_error('nip'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="nik">NIK</label>
                            <input type="text" class="form-control" id="nik" name="nik">
                            <small class="form-text text-danger"><?php echo form_error('nik'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">KTP</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Gelar Depan</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Nama</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Foto</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">gelarBelakang</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Nama Panggilan</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Tempat Lahir</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Tanggal Lahir</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Jenis Kelamin</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Agama</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Alamat KTP</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Provinsi KTP</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Kota KTP</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Kecamatan KTP</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Kelurahan KTP</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Kode Pos KTP</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Alamat Tinggal</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Provinsi Tinggal</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Kota Tinggal</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Kecamatan Tinggal</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Kelurahan Tinggal</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Kode Pos Tinggal</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Email</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">Phone</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Mobile</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Status Perkawinan</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Status Wajib Pajak</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">No Kartu Keluarga</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Scan KK</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        <div class="form-group">
                            <label for="ktp">No NPWP</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Scan NPWP</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">BPJSTK</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">BPJSKES</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Golongan Darah</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Nomor Rekam Medik</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Nomor SIM</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Nomor Rekening</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div><div class="form-group">
                            <label for="ktp">Golongan</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        </div><div class="form-group">
                            <label for="ktp">Profesi</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div></div><div class="form-group">
                            <label for="ktp">Jabatan</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div></div><div class="form-group">
                            <label for="ktp">SOTK</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div></div><div class="form-group">
                            <label for="ktp">MKG</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div></div><div class="form-group">
                            <label for="ktp">Gaji Pokok</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div></div><div class="form-group">
                            <label for="ktp">Unit</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div>
                        </div><div class="form-group">
                            <label for="ktp">Status Kepegawaian</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div></div><div class="form-group">
                            <label for="ktp">Akta Kematian</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div></div><div class="form-group">
                            <label for="ktp">Tanggal Pensiun</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div></div><div class="form-group">
                            <label for="ktp">Tanggal Kematian</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        </div></div><div class="form-group">
                            <label for="ktp">Jenjang Karir</label>
                            <input type="ktp" class="form-control" id="ktp" name="ktp">
                            <small class="form-text text-danger"><?php echo form_error('ktp'); ?></small>
                        
                        <button type="submit" name="tambah" class="btn btn-primary float-right">Tambah Data</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
</div>